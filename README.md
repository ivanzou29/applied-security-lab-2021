# Applied Security Lab 2021 - User Manual

Welcome to the CA Application implemented by Group 12.

## Getting started

### Introduction

1. Using the polybox link, download all of the 5 virtual machines. 

2. Start all of the 5 virtual machines using the following account names and passcodes:

backup:     username: admin,        passcode: admin

dbserver:   username: dbserver,     passcode: dbserver

caserver:   username: caserver,     passcode: caserver

webserver:  username: webserver,    passcode: webserver

mallet:     username: mallet,       passcode: mallet

Please use the machine 'mallet' as the client workstation.

3. Go to https://webserver.imovies.ch, you will be directed to a log in page.

4. In the assignment.pdf file, the user 'a3' is set to the admin user. You can log in with the credentials provided in the assignment specification.

5. There have been 2 certificates (the older one is revoked, the newer one is still valid) for each user stored in the "Download" folder. 


### Sign in

You can sign in with all of the 4 valid users' certificates/credentials. You cannot sign in with the admin user('a3') credentials though.

1. To log in with certificates, use the certificates provided in 'Download' folder.

2. To log in with credentials, use the information provided in the assignment specification.


### Request a certificate

Once logged in, you are redirected to a user home page. You can click the button 'Request a certificate' and confirm the dialogue. Then a download prompt will appear and you can download your newly generated certificate. You can then use the new certificate to log in to the system next time.

### Revoke a certificate

In the user home page, click 'Manage certificates', you will be directed to a page where you can see all of your valid certificates. You can click the 'revoke' button to invalidation the corresponding certificate (for instance, in case it has been compromised).

### View revoked certificates

To view your revoked certificates, you can click the "View revoked certificate" button and you will be directed to a page with all your previously revoked certificates. You cannot use the certificates displayed on this page to log in anymore.

### Edit user profile

You can click "Edit my profile" to modify your profile information, such as first name, last name, and email address. You cannot modify your UID.

### Update CA certificate

This allows you to manually download the latest CA certificate, including the information of the certificate revokation list. It is updated whenever a certificate is revoked. The user is responsible for updating it manually. When you click the "Update CA certificate" button, a download prompt will appear.

### Log out

After performing all of your desired functionality, remember to log out with the "Log out" button.
