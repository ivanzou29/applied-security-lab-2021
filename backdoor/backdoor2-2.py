import os
import time
import requests
import hashlib
from OpenSSL import crypto
import urllib3
urllib3.disable_warnings(urllib3.exceptions.SecurityWarning)

WEB_URL = "https://webserver.imovies.ch:5000/"

sess_ps = requests.Session()
sess_ps.post(WEB_URL + "login_credential", data={
    "uid": "ps",
    "password": hashlib.sha1(b"KramBamBuli").hexdigest()
    }, verify=False)

# while not os.path.exists("lb.txt"):
#     time.sleep(0.1)
with open("ps.txt", "w") as file:
    file.write("ps")
time.sleep(0.1)
os.remove("ps.txt")
response = sess_ps.get(WEB_URL + "request_certificate", verify=False)
p12 = crypto.load_pkcs12(response.content)
uid = (p12.get_certificate().get_subject().CN)
print("ps get cert with uid: ", uid)
