#!/bin/sh

export BORG_REPO=borg@backupserver.imovies.ch:/backup/caserver
export BORG_PASSPHRASE='mhLwxqrZ6bVrzj5'

# some helpers and error handling:
info() { printf "\n%s %s\n\n" "$( date )" "$*" >&2; }
trap 'echo $( date ) Backup interrupted >&2; exit 2' INT TERM

info "Starting backup"

borg create                         \
    --verbose                       \
    --filter AME                    \
    --list                          \
    --stats                         \
    --show-rc                       \
    --compression lz4               \
    --exclude-caches                \
    --exclude '/home/caserver/applied-security-lab-2021/ca_server/CA/private' \
                                    \
    ::'caserver-{now}'             \
    /home/caserver/applied-security-lab-2021/ca_server/CA   \
    /home/caserver/applied-security-lab-2021/ca_server/caserver.log         \

backup_exit=$?

info "Pruning repository"

# Maintain 7 daily, 4 weekly and 6 monthly archives

borg prune                          \
    --list                          \
    --prefix 'caserver-'          \
    --show-rc                       \
    --keep-hourly   24               \
    --keep-daily    7               \
    --keep-weekly   4               \
    --keep-monthly  6               \

prune_exit=$?

# use highest exit code as global exit code
global_exit=$(( backup_exit > prune_exit ? backup_exit : prune_exit ))

if [ ${global_exit} -eq 0 ]; then
    info "Backup and Prune finished successfully"
elif [ ${global_exit} -eq 1 ]; then
    info "Backup and/or Prune finished with warnings"
else
    info "Backup and/or Prune finished with errors"
fi

exit ${global_exit}