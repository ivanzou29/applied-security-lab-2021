#!/usr/bin/env python
# encoding: utf-8
import base64
import json
import linecache
import logging
import os
import secrets
import ssl
import subprocess
import time
from datetime import datetime
from logging.config import dictConfig

from flask import Flask, abort, jsonify
from flask.globals import request
from OpenSSL import crypto

from config import LOG_FILE, LOGGING_CONFIG

logging.basicConfig(filename=LOG_FILE, level=logging.DEBUG)
dictConfig(LOGGING_CONFIG)

CA_SERVER_KEY_FILE = "caserverkey.pem"
CA_SERVER_CERT_FILE = "caservercert.pem"
CA_ISSUER_CERT_FILE = "CA/caissuercert.pem"
REVOKED_CERT_FILE = "CA/revoked.pem"
TO_VERIFY_FILE = "verify.pem"
CSR_FILE = "tmp.csr"
OPENSSL_CONF_FILE = "CA/openssl.cnf"
CRL_FILE = "CA/crl/crl.pem"
ROOT_CA_FILE = "CA/certs/rootcert.pem"
PRIV_KEY_DIR = "CA/private"
INDEX_DB_FILE = "CA/index.txt"
WEB_SERVER_IP = "192.168.100.10"
BACKUP_SERVER_IP = "192.168.100.13"
BACKUP_PUBKEY_FILE = "./backuppubkey.pem"

app = Flask(__name__)
handler = logging.FileHandler(LOG_FILE, encoding='UTF-8')
handler.setLevel(logging.DEBUG)

logging_format = logging.Formatter(
    '[%(asctime)s] %(levelname)s in %(module)s: %(message)s'
)
handler.setFormatter(logging_format)
app.logger.addHandler(handler)


def create_cert(username, email):
    k = crypto.PKey()
    k.generate_key(crypto.TYPE_RSA, 4096)

    cert_req = crypto.X509Req()
    cert_req.get_subject().C = "CH"
    cert_req.get_subject().ST = "ZH"
    cert_req.get_subject().L = "Zurich"
    cert_req.get_subject().O = "iMovies"
    cert_req.get_subject().OU = "iMovies"
    cert_req.get_subject().CN = username
    cert_req.get_subject().emailAddress = email

    cert_req.set_pubkey(k)
    cert_req.sign(k, 'sha256')

    with open(CSR_FILE, "w") as file:
        file.write(crypto.dump_certificate_request(
            crypto.FILETYPE_PEM, cert_req).decode())

    os.system(
        "openssl ca -batch -in {} -config {} > /dev/null".format(CSR_FILE, OPENSSL_CONF_FILE))

    start_time = time.time()
    private_key_file = "CA/private/temkey.pem"
    with open(private_key_file, "wb") as file:
        file.write(crypto.dump_privatekey(crypto.FILETYPE_PEM, k))

    enc_priv_file = private_key_file + ".enc"
    pwd_priv_file = private_key_file + ".pwd"

    random_pwd = secrets.token_urlsafe(32)
    os.system("echo '{}' | openssl enc -aes-256-ctr -pbkdf2 -k - -in {} -out {}".format(
        random_pwd, private_key_file, enc_priv_file
    ))
    os.system("echo '{}' | openssl rsautl -encrypt -in - -out {} -inkey {} -pubin".format(
        random_pwd, pwd_priv_file, BACKUP_PUBKEY_FILE
    ))

    backup_priv_file = "priv_{}_{}.pem".format(
        username, datetime.now().strftime("%Y-%m-%d_%H:%M:%S"))
    os.system('scp -i /home/caserver/.ssh/backupserver {} borg@backupserver.imovies.ch:/backup/priv_key/{}'.format(
        enc_priv_file, backup_priv_file + ".enc"))
    os.system('scp -i /home/caserver/.ssh/backupserver {} borg@backupserver.imovies.ch:/backup/priv_key/{}'.format(
        pwd_priv_file, backup_priv_file + ".pwd"))
    end_time = time.time()
    print("Consuming {}s".format(end_time - start_time))

    with open("CA/serial.old", "r") as file:
        serial = file.read().strip()

    cert_file = "CA/newcerts/{}.pem".format(serial)
    pkcs_file = "CA/private/{}.p12".format(serial)

    os.system("openssl pkcs12 -export -out {} -inkey {} -in {} -passout pass: -certfile CA/revoked.pem > /dev/null".format(
        pkcs_file, private_key_file, cert_file))

    with open(pkcs_file, "rb") as file:
        p12 = file.read()

    app.logger.info(
        'A certificate for {} / {} was created!'.format(username, email))

    os.remove(CSR_FILE)
    os.remove(private_key_file)
    os.remove(pkcs_file)

    return {
        "pkcs12": base64.encodebytes(p12).decode("utf-8"),
        "serial": int(serial, 16),
    }


@app.route('/request', methods=['GET'])
def request_cert():
    if request.remote_addr != WEB_SERVER_IP:
        abort(404)

    username = request.args.get("username")
    email = request.args.get("email")
    # print(username)
    if username:
        key_cert_pair = create_cert(username, email)
        app.logger.info(
            'A certificate for {} / {} was issued!'.format(username, email))
        return jsonify(key_cert_pair)
    else:
        app.logger.info('Certificate request failed!')
        abort(404)


@app.route('/revoke', methods=['GET'])
def revoke_cert():
    if request.remote_addr != WEB_SERVER_IP:
        app.logger.info('Certificate revokation failed!')
        abort(404)

    json_dict = request.get_json()
    revoke_serial = json_dict["serial"]

    TO_REVOKE_FILE = "CA/newcerts/{}.pem".format(revoke_serial)
    os.system(
        "openssl ca -revoke {} -config {}".format(TO_REVOKE_FILE, OPENSSL_CONF_FILE))
    os.system(
        "openssl ca -gencrl -out {} -config {}".format(CRL_FILE, OPENSSL_CONF_FILE))
    os.system("cat {} {} {} > {}".format(ROOT_CA_FILE,
                                         CA_ISSUER_CERT_FILE, CRL_FILE, REVOKED_CERT_FILE))

    with open(REVOKED_CERT_FILE, "r") as file:
        new_cert = file.read()

    app.logger.info(
        'Certificate with serial number: {} was revoked.'.format(revoke_serial))

    return jsonify({"new_cert": new_cert})


def verify_cert(cert_file):
    p = subprocess.Popen(["openssl", "verify", "-CAfile",  REVOKED_CERT_FILE,
                         "-crl_check", cert_file], stdout=subprocess.PIPE)
    res = p.stdout.read()
    return res


def parse_ca_time(date: str):
    date = date[:-1]
    d = datetime.strptime(date, "%y%m%d%H%M%S")
    return d.ctime()


@app.route('/query', methods=['POST'])
def query():
    if request.remote_addr != WEB_SERVER_IP:
        app.logger.info('Query failed.')
        abort(404)

    linecache.checkcache(INDEX_DB_FILE)
    serial_list = request.get_json()["serial"]
    valid_list = []
    revoked_list = []
    expired_list = []
    for serial in serial_list:
        serial = int(serial)
        line = linecache.getline(INDEX_DB_FILE, serial)
        if len(line) == 0:
            continue
        line = line.split()
        flag = line[0]
        expired_date = parse_ca_time(line[1])
        if flag == "V":
            serial = line[2]
            valid_list.append({
                "expired_date": expired_date,
                "serial": serial,
            })
        elif flag == "R":
            revoked_date = parse_ca_time(line[2])
            serial = line[3]
            revoked_list.append({
                "expired_date": expired_date,
                "revoked_date": revoked_date,
                "serial": serial
            })
        elif flag == "E":
            serial = line[2]
            expired_list.append({
                "expired_date": expired_date,
                "serial": serial,
            })
    app.logger.info('Successfully queried the certificate information.')
    return jsonify({
        "valid": valid_list,
        "revoked": revoked_list,
        "expired": expired_list
    })


@app.route('/admin', methods=['GET'])
def admin():
    if request.remote_addr != WEB_SERVER_IP:
        abort(404)

    valid_cnt = 0
    revoked_cnt = 0
    expired_cnt = 0
    with open("CA/index.txt", "r") as file:
        for line in file:
            flag = line[0]
            if flag == "V":
                valid_cnt += 1
            elif flag == "R":
                revoked_cnt += 1
            elif flag == "E":
                expired_cnt += 1

    with open("CA/serial.old", "r") as file:
        serial = file.read().strip()
    app.logger.info('Successfully queried the admin information.')
    return jsonify({
        "valid": valid_cnt,
        "revoked": revoked_cnt,
        "expired": expired_cnt,
        "serial": serial
    })


@app.route('/backup', methods=['GET'])
def backup():
    if request.remote_addr != BACKUP_SERVER_IP:
        app.logger.info(
            'An invalid server has request for backup, be cautious.')
        abort(404)

    backup_dict = dict()

    priv_key_list = os.listdir(PRIV_KEY_DIR)
    p12_dict = dict()
    for priv_key in priv_key_list:
        if priv_key.endswith(".p12.enc") or priv_key.endswith(".p12.pwd"):
            p12_file = os.path.join(PRIV_KEY_DIR, priv_key)
            with open(p12_file, "rb") as file:
                p12 = file.read()
            p12_dict[priv_key] = (base64.encodebytes(p12).decode("utf-8"))
            os.remove(p12_file)

    with open(LOG_FILE, 'rb') as log_file:
        log_content = log_file.read()

    backup_dict['p12_dict'] = p12_dict
    backup_dict['log'] = log_content

    app.logger.info('Keys and log were updated to the backup server.')
    return jsonify(backup_dict)


@app.route('/auth', methods=['POST'])
def auth():
    if request.remote_addr != WEB_SERVER_IP:
        abort(404)

    json_dict = request.get_json()
    msg_dict = json_dict["msg"]
    send_time = msg_dict["time"] / 1000.0
    curr_time = time.time()
    if curr_time - send_time > 10:
        app.logger.info('Authentication failed: expired request.')
        abort(404)

    serial = msg_dict["serial"]
    nonce = msg_dict["nonce"]

    serial = int(serial, 16)
    linecache.checkcache(INDEX_DB_FILE)
    line = linecache.getline(INDEX_DB_FILE, serial)
    line = line.split()
    flag = line[0]
    if flag == "V":
        serial = line[2]
    else:
        app.logger.info('Authentication failed: revoked cert.')
        return jsonify({"error": "Expired/Revoked cert"})
    cert_file = "CA/newcerts/{}.pem".format(serial)
    res = verify_cert(cert_file)
    if b"OK" not in res:
        app.logger.info('Authentication failed: revoked cert.')
        return jsonify({"error": "Expired/Revoked cert"})

    signature = json_dict["signature"]
    signature = base64.decodebytes(signature.encode("utf-8"))
    msg = json.dumps(msg_dict, separators=(',', ':'))
    with open(cert_file, "rb") as file:
        client_cert = crypto.load_certificate(crypto.FILETYPE_PEM, file.read())
    try:
        crypto.verify(client_cert, signature, msg.encode("utf-8"), "sha256")
    except Exception:
        app.logger.info('Authentication failed: invalid signature.')
        return jsonify({"error": "Invalid signature"})
    username = client_cert.get_subject().CN

    app.logger.info('Successfully authenticated the user {}.'.format(username))

    return jsonify({"authenticated": username})


if __name__ == "__main__":
    context = ssl.create_default_context(
        purpose=ssl.Purpose.CLIENT_AUTH, cafile=ROOT_CA_FILE)
    context.load_cert_chain(certfile=CA_SERVER_CERT_FILE,
                            keyfile=CA_SERVER_KEY_FILE)
    context.verify_mode = ssl.CERT_REQUIRED
    context.check_hostname = False

    app.run(host="caserver.imovies.ch", port=5001, ssl_context=context)
