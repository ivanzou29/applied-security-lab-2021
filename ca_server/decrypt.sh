#!/bin/sh

# echo "123" | openssl rsautl -encrypt -in - -out ctxt -inkey backuppubkey.pem -pubin
# echo "123" | openssl enc -aes-256-ctr -pbkdf2 -k - -in test.p12 -out test.p12.enc

openssl rsautl -decrypt -in $1 -inkey backupkey.pem | openssl enc -aes-256-ctr -pbkdf2 -d -k - -in $2 -out restore.p12
# echo "123" | openssl enc -aes-256-ctr -pbkdf2 -d -k - -in test.p12.enc -out restore.p12
openssl pkcs12 -info -in restore.p12
