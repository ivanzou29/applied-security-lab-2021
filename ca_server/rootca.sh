#!/bin/sh
openssl req -x509 -extensions v3_ca -subj "/C=CH/ST=ZH/L=Zurich/O=iMovies/OU=iMovies/CN=root.imovies.ch/emailAddress=root@imovies.ch" -days 3650 -newkey ec:<(openssl ecparam -name secp521r1) -keyout rootkey.pem -out rootcert.pem

openssl req -subj "/C=CH/ST=ZH/L=Zurich/O=iMovies/OU=iMovies/CN=caserver.imovies.ch/emailAddress=caserver@imovies.ch" -days 3650 -nodes -newkey ec:<(openssl ecparam -name secp521r1) -keyout caserverkey.pem -out caserver.csr

openssl ca -batch -in caserver.csr -config openssl.cnf

openssl req -subj "/C=CH/ST=ZH/L=Zurich/O=iMovies/OU=iMovies/CN=webserver.imovies.ch/emailAddress=webserver@imovies.ch" -days 3650 -nodes -newkey ec:<(openssl ecparam -name secp521r1) -keyout webserverkey.pem -out webserver.csr

openssl ca -batch -in webserver.csr -config openssl.cnf

openssl req -subj "/C=CH/ST=ZH/L=Zurich/O=iMovies/OU=iMovies/CN=backupserver.imovies.ch/emailAddress=backupserver@imovies.ch" -days 3650 -nodes -newkey ec:<(openssl ecparam -name secp521r1) -keyout backupserverkey.pem -out backupserver.csr

openssl ca -batch -in backupserver.csr -config openssl.cnf

openssl req -extensions v3_ca -subj "/C=CH/ST=ZH/L=Zurich/O=iMovies/OU=iMovies/CN=caissuer.imovies.ch/emailAddress=caissuer@imovies.ch" -days 3650 -nodes -newkey ec:<(openssl ecparam -name secp521r1) -keyout caissuerkey.pem -out caissuer.csr

openssl ca -extensions v3_ca -batch -in caissuer.csr -config openssl.cnf

openssl req -subj "/C=CH/ST=ZH/L=Zurich/O=iMovies/OU=iMovies/CN=dbserver.imovies.ch/emailAddress=dbserver@imovies.ch" -days 3650 -nodes -newkey ec:<(openssl ecparam -name secp521r1) -keyout dbserverkey.pem -out dbserver.csr

openssl ca -batch -in dbserver.csr -config openssl.cnf


cat rootcert.pem newcerts/01.pem > caserverchaincert.pem
cat rootcert.pem newcerts/02.pem > webserverchaincert.pem
cat rootcert.pem newcerts/03.pem > caissuerchaincert.pem
cat rootcert.pem newcerts/04.pem > dbserverchaincert.pem
