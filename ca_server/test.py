import base64
import requests
import random
import string

CA_SERVER_CERT_FILE = "caserverchaincert.pem"
CLIENT_KEY_FILE = "webserverkey.pem"
CLIENT_CERT_FILE = "webservercert.pem"
URL = "https://caserver.imovies.ch"
PORT = 5001

def request_cert(username):
    # username = "".join([random.choice(string.ascii_letters) for _ in range(10)])
    # username = "alice"
    response = requests.get("{}:{}/request".format(URL, PORT),
        params={"username": username}, verify=CA_SERVER_CERT_FILE,
        cert=(CLIENT_CERT_FILE, CLIENT_KEY_FILE))
    json_dict = (response.json())
    print(json_dict)
    p12 = json_dict["pkcs12"]
    with open("test.p12", "wb") as file:
        file.write(base64.decodebytes(p12.encode("utf-8")))

# request_cert()

def revoke_cert(serial_number):
    # with open("testcert.pem", "r") as file:
        # cert = file.read()
    response = requests.get("{}:{}/revoke".format(URL, PORT),
        json={"serial": serial_number}, verify=CA_SERVER_CERT_FILE,
        cert=(CLIENT_CERT_FILE, CLIENT_KEY_FILE))
    # print(response.content)

# revoke_cert()

def verify_cert():
    with open("testcert.pem", "r") as file:
        cert = file.read()
    response = requests.get("{}:{}/verify".format(URL, PORT),
        json={"certificate": cert}, verify=CA_SERVER_CERT_FILE,
        cert=(CLIENT_CERT_FILE, CLIENT_KEY_FILE))
    print(response.content)

def query():
    response = requests.get("{}:{}/query".format(URL, PORT),
        params={"username": "alice"}, verify=CA_SERVER_CERT_FILE,
        cert=(CLIENT_CERT_FILE, CLIENT_KEY_FILE))
    print(response.content)

def admin():
    response = requests.get("{}:{}/admin".format(URL, PORT),
        verify=CA_SERVER_CERT_FILE,
        cert=(CLIENT_CERT_FILE, CLIENT_KEY_FILE))
    print(response.content)

def backup():
    response = requests.get("{}:{}/backup".format(URL, PORT),
        verify=CA_SERVER_CERT_FILE,
        cert=(CLIENT_CERT_FILE, CLIENT_KEY_FILE))
    print(response.json().keys())

# request_cert("alice")
# request_cert("alice")
# request_cert("alice")
# revoke_cert("03")
# verify_cert()
# revoke_cert()
# verify_cert()
query()
# admin()
# backup()