#!/bin/sh

export BORG_REPO=borg@backupserver.imovies.ch:/backup/dbserver
export BORG_PASSPHRASE='Ntm53rKLEMbkGW2'

# some helpers and error handling:
info() { printf "\n%s %s\n\n" "$( date )" "$*" >&2; }
trap 'echo $( date ) Backup interrupted >&2; exit 2' INT TERM

info "Starting backup"

mysqldump imovies -u borgbackup -pborgbackup > /home/borgbackup/imovies_dump.sql

borg create                         \
    --verbose                       \
    --filter AME                    \
    --list                          \
    --stats                         \
    --show-rc                       \
    --compression lz4               \
    --exclude-caches                \
                                    \
    ::'dbserver-{now}'             \
    /home/borgbackup/imovies_dump.sql   \
    /home/dbserver/applied-security-lab-2021/dbserver/dbserver.log         \

backup_exit=$?

info "Pruning repository"

# Maintain 7 daily, 4 weekly and 6 monthly archives

borg prune                          \
    --list                          \
    --prefix 'dbserver-'          \
    --show-rc                       \
    --keep-hourly   24               \
    --keep-daily    7               \
    --keep-weekly   4               \
    --keep-monthly  6               \

prune_exit=$?

# use highest exit code as global exit code
global_exit=$(( backup_exit > prune_exit ? backup_exit : prune_exit ))

if [ ${global_exit} -eq 0 ]; then
    info "Backup and Prune finished successfully"
elif [ ${global_exit} -eq 1 ]; then
    info "Backup and/or Prune finished with warnings"
else
    info "Backup and/or Prune finished with errors"
fi

exit ${global_exit}