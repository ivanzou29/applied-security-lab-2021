# import mysql.connector
import MySQLdb

class DBConnector():
    def __init__(self):
        self.cnx = MySQLdb.connect(user='dbserver', password='dbserver', host='localhost', database='imovies')

        self.cursor = self.cnx.cursor()

    def __del__(self):
        self.cnx.commit()
        self.cnx.close()

    def get_password_by_uid(self, uid):
        query = ("SELECT pwd FROM users WHERE uid = %(uid)s;")
        self.cursor.execute(query, {"uid": uid})
        pwd = self.cursor.fetchone()
        if pwd is not None:
            return pwd[0]
        else:
            return None

    def add_certificate(self, serial, uid):
        query = ("INSERT INTO certificates VALUES (%(serial)s, %(uid)s);".format(serial, uid))
        try:
            self.cursor.execute(query, {"serial": serial, "uid": uid})
            self.cnx.commit()
            return True
        except Exception:
            self.cnx.rollback()
            return False

    def get_serial_number_by_uid(self, uid):
        query = ("SELECT serialnumber FROM certificates WHERE uid = %(uid)s")
        self.cursor.execute(query, {"uid": uid})
        serial_list = []
        for serial_number in self.cursor:
            serial_list.append(serial_number[0])
        return serial_list

    def get_profile_by_uid(self, uid):
        query = ("SELECT uid, firstname, lastname, email FROM users WHERE uid = %(uid)s;")
        self.cursor.execute(query, {"uid": uid})
        profile = self.cursor.fetchone()
        return profile

    def update_profile_by_uid(self, uid, first_name, last_name, email):
        query = ("UPDATE users SET firstname = %(firstname)s, lastname = %(lastname)s, email = %(email)s WHERE uid = %(uid)s;")
        try:
            self.cursor.execute(query, {"firstname": first_name, "lastname": last_name, "email": email, "uid": uid})
            self.cnx.commit()
            return True
        except Exception:
            self.cnx.rollback()
            return True
