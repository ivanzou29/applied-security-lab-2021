from config import LOG_FILE, LOGGING_CONFIG
from logging.config import dictConfig
import logging
import ssl
from flask import Flask, request, jsonify, abort

from db_connector import DBConnector

DB_SERVER_KEY_FILE = "dbserverkey.pem"
DB_SERVER_CERT_FILE = "dbservercert.pem"
ROOT_CA_FILE = "rootcert.pem"
WEB_SERVER_IP = "192.168.100.10"
BACKUP_SERVER_IP = "192.168.100.13"

logging.basicConfig(filename=LOG_FILE, level=logging.DEBUG)
dictConfig(LOGGING_CONFIG)

db_connector = DBConnector()
app = Flask(__name__)
handler = logging.FileHandler(LOG_FILE, encoding='UTF-8')
handler.setLevel(logging.DEBUG)

logging_format = logging.Formatter(
    '[%(asctime)s] %(levelname)s in %(module)s: %(message)s'
)
handler.setFormatter(logging_format)
app.logger.addHandler(handler)


@app.route('/get_pwd_hash', methods=['GET'])
def get_password_hash():
    if request.remote_addr != WEB_SERVER_IP:
        app.logger.info(
            'Invalid query source wanted to get the hashed password. Be cautious!')
        abort(404)

    uid = request.args.get("uid")
    pwd_hash = db_connector.get_password_by_uid(uid)
    app.logger.info(
        'Successfully queried hashed password for user {}.'.format(uid))
    return jsonify({"pwd_hash": pwd_hash})


@app.route('/add_cert', methods=['GET'])
def add_cert():
    if request.remote_addr != WEB_SERVER_IP:
        app.logger.info(
            'Invalid query source wanted to add a certificate to the database. Be cautious!')
        abort(404)

    serial = request.args.get("serial")
    uid = request.args.get("uid")
    if db_connector.add_certificate(serial, uid):
        app.logger.info(
            'Successfully added certificate-{} for user {}.'.format(serial, uid))
        return "OK"
    else:
        app.logger.info(
            'Failed to add certificate-{} for user {}.'.format(serial, uid))
        return "Failed"


@app.route('/get_serial', methods=['GET'])
def get_serial_number():
    if request.remote_addr != WEB_SERVER_IP:
        app.logger.info(
            'Invalid query source wanted to get the serial numbers of certificates from a certain UID. Be cautious!')
        abort(404)

    uid = request.args.get("uid")
    serial_list = db_connector.get_serial_number_by_uid(uid)

    app.logger.info(
        'Successfully queried serial numbers of certificates for user {}.'.format(uid))
    return jsonify({"serial": serial_list})


@app.route('/get_profile', methods=['GET'])
def get_profile():
    if request.remote_addr != WEB_SERVER_IP:
        app.logger.info(
            'Invalid query source wanted to get the user profile information. Be cautious!')
        abort(404)

    uid = request.args.get("uid")
    profile_list = db_connector.get_profile_by_uid(uid)

    app.logger.info(
        'Successfully queried the user profile for user {}.'.format(uid))
    return jsonify({"profile": profile_list})


@app.route('/update_profile', methods=['GET'])
def update_profile():
    if request.remote_addr != WEB_SERVER_IP:
        app.logger.info(
            'Invalid query source wanted to update the user information. Be cautious!')
        abort(404)

    uid = request.args.get("uid")
    first_name = request.args.get("firstname")
    last_name = request.args.get("lastname")
    email = request.args.get("email")
    if db_connector.update_profile_by_uid(uid, first_name, last_name, email):
        app.logger.info('Successfully updated the profile for user {}, with first name: {}, last name: {}, email: {}.'.format(
            uid, first_name, last_name, email))
        return "OK"
    else:
        app.logger.info(
            'Failed to update the profile for user {}, with first name: {}, last name: {}, email: {}.'.format(uid, first_name, last_name, email))
        return "Failed"


@app.route('/backup', methods=['GET'])
def backup():
    if request.remote_addr != BACKUP_SERVER_IP:
        app.logger.info(
            'An invalid server has request for backup, be cautious.')
        abort(404)

    return "backup"


if __name__ == "__main__":
    context = ssl.create_default_context(
        purpose=ssl.Purpose.CLIENT_AUTH, cafile=ROOT_CA_FILE)
    context.load_cert_chain(certfile=DB_SERVER_CERT_FILE,
                            keyfile=DB_SERVER_KEY_FILE)
    context.verify_mode = ssl.CERT_REQUIRED
    context.check_hostname = False

    # app.run(host='0.0.0.0')
    app.run(host="dbserver.imovies.ch", port=5002, ssl_context=context)
