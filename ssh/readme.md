### SSH

On mallet:

- webserver: `ssh webserver`

- caserver:

  - use webserver as forwarder: `ssh -L 2323:caserver.imovies.ch:22 webserver`
  - open another terminal: `ssh caserver`

- dbserver:

  - use webserver as forwarder: `ssh -L 2424:dbserver.imovies.ch:22 webserver`
  - open another terminal: `ssh dbserver`

- backupserver:

  - use webserver as forwarder: `ssh -L 2525:backupserver.imovies.ch:22 webserver`
  - open another terminal: `ssh backupserver`
  
