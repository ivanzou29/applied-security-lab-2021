import functools
import db_funcs
from flask import session, request, redirect, url_for

def check_password(uid, hashed_password):
    correct_password = db_funcs.get_password_by_uid(uid)
    if correct_password == hashed_password:
        return True
    else:
        return False

def requires_authentication(f):
    @functools.wraps(f)
    def inner(*args, **kwargs):
        if 'uid' in session:
            return f(*args, **kwargs)
        else:
            return redirect(url_for('login_credential'))
        return inner

# a3 is set as the default admin
def is_admin(uid):
    return uid == 'a3'