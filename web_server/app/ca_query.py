import requests
from config import CA_SERVER_URL, CA_SERVER_PORT, CA_SERVER_CERT_FILE, CLIENT_CERT_FILE, CLIENT_KEY_FILE
'''
This module contains functions to query the CA server for the certificate related information.
'''
'''
response = requests.get(
    "{}:{}/request".format(CA_SERVER_URL, CA_SERVER_PORT),
    params={"username": uid},
    verify=CA_SERVER_CERT_FILE,
    cert=(CLIENT_CERT_FILE, CLIENT_KEY_FILE)
)
json_dict = (response.json())
p12 = json_dict["pkcs12"]
'''
def get_valid_certificates_by_serial(serial_list):
    response = requests.post(
        "{}:{}/query".format(CA_SERVER_URL, CA_SERVER_PORT),
        json={"serial": serial_list},
        verify=CA_SERVER_CERT_FILE,
        cert=(CLIENT_CERT_FILE, CLIENT_KEY_FILE)
    )

    json_dict = response.json()
    return json_dict['valid']


def get_revoked_certificates_by_serial(serial_list):
    response = requests.post(
        "{}:{}/query".format(CA_SERVER_URL, CA_SERVER_PORT),
        json={"serial": serial_list},
        verify=CA_SERVER_CERT_FILE,
        cert=(CLIENT_CERT_FILE, CLIENT_KEY_FILE)
    )

    json_dict = response.json()
    return json_dict['revoked']

def revoke_certificate(serial):
    response = requests.get(
        "{}:{}/revoke".format(CA_SERVER_URL, CA_SERVER_PORT),
        json={"serial": serial},
        verify=CA_SERVER_CERT_FILE,
        cert=(CLIENT_CERT_FILE, CLIENT_KEY_FILE)
    )

    json_dict = response.json()
    return json_dict

def admin_query():
    response = requests.get(
        "{}:{}/admin".format(CA_SERVER_URL, CA_SERVER_PORT),
        verify=CA_SERVER_CERT_FILE,
        cert=(CLIENT_CERT_FILE, CLIENT_KEY_FILE)
    )
    json_dict = response.json()
    return json_dict
