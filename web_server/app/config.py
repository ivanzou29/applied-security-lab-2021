CA_SERVER_URL = "https://caserver.imovies.ch"
CA_SERVER_PORT = 5001
CA_SERVER_CERT_FILE = "./certs/caserverchaincert.pem"
CLIENT_CERT_FILE = "./certs/webservercert.pem"
CLIENT_KEY_FILE = "./certs/webserverkey.pem"
WEB_SERVER_CERT_FILE = CLIENT_CERT_FILE
WEB_SERVER_KEY_FILE = CLIENT_KEY_FILE
BACKUP_SERVER_IP = "192.168.100.13"
LOG_FILE = "webserver.log"
REVOKED_CERT_FILE = "./certs/revoked.pem"

LOGGING_CONFIG = {
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
}