import requests
# from werkzeug.wrappers import response
from config import CLIENT_CERT_FILE, CLIENT_KEY_FILE


DB_URL = "https://dbserver.imovies.ch:5002/"
# VERIFY_MODE = False
VERIFY_MODE = "./certs/dbserverchaincert.pem"
# CLIENT_CERT = None
CLIENT_CERT = (CLIENT_CERT_FILE, CLIENT_KEY_FILE)

def get_password_by_uid(uid):
    response = requests.get(DB_URL + "get_pwd_hash",
        {"uid": uid}, verify=VERIFY_MODE, cert=CLIENT_CERT)
    hashed_password = response.json()["pwd_hash"]
    return hashed_password

def get_profile_by_uid(uid):
    response = requests.get(DB_URL + "get_profile",
        {"uid": uid}, verify=VERIFY_MODE, cert=CLIENT_CERT)
    uid, firstname, lastname, email = response.json()["profile"]
    return uid, firstname, lastname, email

def update_profile_by_uid(uid, first_name, last_name, email):
    response = requests.get(DB_URL + "update_profile",
        {"uid": uid, "firstname": first_name, "lastname": last_name, "email": email},
        verify=VERIFY_MODE, cert=CLIENT_CERT)
    if (b"OK" not in response.content):
        print("Update profile failed!")
    return

def get_certificates_id_by_uid(uid):
    response = requests.get(DB_URL + "get_serial",
        {"uid": uid}, verify=VERIFY_MODE, cert=CLIENT_CERT)
    serial_list = response.json()["serial"]
    return serial_list

def add_certificate(serial, uid):
    response = requests.get(DB_URL + "add_cert",
        {"serial": serial, "uid": uid},
        verify=VERIFY_MODE, cert=CLIENT_CERT)
    if (b"OK" not in response.content):
        print("Add cert failed!")
    return
