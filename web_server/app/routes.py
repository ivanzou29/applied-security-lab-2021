import base64
import datetime as dt
import os

import requests
from flask import Flask, render_template, request, redirect, url_for, session, send_file, abort, jsonify

import auth
import secrets
import ca_query
import db_funcs
from config import CA_SERVER_URL, CA_SERVER_PORT, CA_SERVER_CERT_FILE, CLIENT_CERT_FILE, CLIENT_KEY_FILE, \
	WEB_SERVER_CERT_FILE, WEB_SERVER_KEY_FILE, BACKUP_SERVER_IP, LOGGING_CONFIG, LOG_FILE, REVOKED_CERT_FILE

import logging
from logging.config import dictConfig
logging.basicConfig(filename=LOG_FILE, level=logging.DEBUG)

import urllib3
urllib3.disable_warnings(urllib3.exceptions.SubjectAltNameWarning)

# CA_SERVER_URL = "https://caserver.imovies.ch"
# CA_SERVER_PORT = 5001
# CA_SERVER_CERT_FILE = "./certs/caserverchaincert.pem"
# CLIENT_CERT_FILE = "./certs/webservercert.pem"
# CLIENT_KEY_FILE = "./certs/webserverkey.pem"

dictConfig(LOGGING_CONFIG)

app = Flask(__name__)
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.config['PERMANENT_SESSION_LIFETIME'] = dt.timedelta(minutes=5)

handler = logging.FileHandler(LOG_FILE, encoding='UTF-8')
handler.setLevel(logging.DEBUG)

logging_format = logging.Formatter(
	'[%(asctime)s] %(levelname)s in %(module)s: %(message)s'
)
handler.setFormatter(logging_format)
app.logger.addHandler(handler)

app.secret_key = secrets.token_urlsafe(32)

@app.route('/')
def default():
	return redirect(url_for('home'))

@app.route('/home')
def home():
	if 'uid' not in session:
		return redirect(url_for('login_credential'))
	else:
		if auth.is_admin(session['uid']):
			return redirect(url_for('admin_home'))
		else:
			return redirect(url_for('user_home'))

@app.route('/login_certificate', methods=['GET', 'POST'])
def login_certificate():
	if request.method == 'GET':
		if 'uid' in session:
			app.logger.info('User {} redirected to homepage.'.format(session['uid']))
			if auth.is_admin(session['uid']):
				return redirect(url_for('admin_home'))
			else:
				return redirect(url_for('user_home'))
		app.logger.info('Login certificate page accessed.')
		return render_template('login_certificate.html')

	elif request.method == 'POST':
		response = requests.post("{}:{}/auth".format(CA_SERVER_URL, CA_SERVER_PORT),
			json=request.get_json(), verify=CA_SERVER_CERT_FILE,
			cert=(CLIENT_CERT_FILE, CLIENT_KEY_FILE))

		json_dict = response.json()
		if "authenticated" in json_dict.keys():
			session['uid'] = json_dict["authenticated"]
			uid = session['uid']

			if auth.is_admin(uid):
				app.logger.info('User {} logged in successfully!'.format(uid))
				return redirect(url_for('admin_home'))
			else:
				app.logger.info('User {} logged in successfully!'.format(uid))
				return redirect(url_for('user_home'))
		else:
			error = "Invalid certificate! Please try again with another one."
			app.logger.info('Someone tried to log in with a certificate but failed.')
			return render_template('login_certificate.html', error=error)

@app.route('/login_credential', methods=['GET', 'POST'])
def login_credential():
	if request.method == 'GET':
		if 'uid' in session:
			app.logger.info('User {} redirected to homepage.'.format(session['uid']))
			return redirect(url_for('user_home'))
		app.logger.info('Login credential page accessed.')
		return render_template('login_credential.html')
	elif request.method == 'POST':
		uid = request.form['uid']
		password = request.form['password']
		if not auth.check_password(uid, password):
		# if request.form['uid'] != 'ivan' or request.form['password'] != 'bf9661defa3daecacfde5bde0214c4a439351d4d':
			error = 'Invalid Credentials. Please try again.'
			app.logger.info('User {} tried to log in with credentials but failed'.format(uid))
			return render_template('login_credential.html', error=error)
		else:
			if auth.is_admin(request.form['uid']):
				error = 'Invalid Credentials. Please try again.'
				app.logger.info('User {} tried to log in with credentials but failed'.format(uid))
				return render_template('login_credential.html', error=error)
			session['uid'] = request.form['uid']

			app.logger.info('User {} logged in successfully!'.format(uid))
			return redirect(url_for('user_home'))

@app.route('/admin_home', methods=['GET'])
def admin_home():
	# button usage:
	# "request a certificate": download directly with pop up message
	# "manage certificates": enter a new html displaying all certificates
	# "view revoked certificates":
	if 'uid' in session:
		uid = session['uid']
		if auth.is_admin(uid):
			app.logger.info('User {} accessed home successfully!'.format(uid))
			admin_dict = ca_query.admin_query()
			return render_template('admin_home.html', admin_dict=admin_dict)
		else:
			app.logger.info('User {} accessed home failed!'.format(uid))
			error = 'To access the admin home, please log out and use your admin certificate to log in.'
			return render_template('user_home.html', error=error)
	app.logger.info('Someone redirected to login certificate page.')
	return redirect(url_for('login_certificate'))

@app.route('/user_home')
def user_home():
	if 'uid' in session:
		uid = session['uid']
		app.logger.info('User {} accessed home successfully!'.format(uid))
		return render_template('user_home.html', uid=uid)
	app.logger.info('Someone redirected to login credential page.')
	return redirect(url_for('login_credential'))

@app.route('/request_certificate')
def request_certificate():
	if 'uid' in session:
		uid = session['uid']
		uid, firstname, lastname, email = db_funcs.get_profile_by_uid(uid)
		response = requests.get(
			"{}:{}/request".format(CA_SERVER_URL, CA_SERVER_PORT),
			params={"username": uid, "email": email},
			verify=CA_SERVER_CERT_FILE,
			cert=(CLIENT_CERT_FILE, CLIENT_KEY_FILE)
		)
		json_dict = (response.json())
		p12 = json_dict["pkcs12"]
		serial = json_dict["serial"]
		db_funcs.add_certificate(serial, uid)

		timestamp = str(dt.datetime.now())

		if not os.path.exists("temp"):
			os.mkdir("temp")
		temp_file = 'temp/cert_{}_{}.p12'.format(uid, timestamp)

		with open(temp_file, "wb") as file:
			file.write(base64.decodebytes(p12.encode("utf-8")))

		download = send_file(temp_file, as_attachment=True)
		os.remove(temp_file)

		app.logger.info('User {} requested a new certificate.'.format(uid))

		return download

	app.logger.info('Someone redirected to login credential page.')
	return redirect(url_for('login_credential'))


@app.route('/manage_certificates', methods=['GET', 'POST'])
def manage_certificates():
	if 'uid' in session:
		uid = session['uid']
		if request.method == 'GET':
			serial_list = db_funcs.get_certificates_id_by_uid(uid)
			my_certificates = ca_query.get_valid_certificates_by_serial(serial_list)

			app.logger.info('User {} accessed the manage certificate page.'.format(uid))

			return render_template('manage_certificates.html', my_certificates=my_certificates)
		elif request.method == 'POST':
			serial = request.form['certID']

			response_json_dict = ca_query.revoke_certificate(serial)

			new_cert = response_json_dict['new_cert']

			with open(REVOKED_CERT_FILE, "w") as file:
				file.write(new_cert)

			app.logger.info('User {} revoked the certificate (serial number: {})'.format(uid, serial))
			return redirect(url_for('manage_certificates'))

	app.logger.info('Someone redirected to login credential page.')
	return redirect(url_for('login_credential'))


@app.route('/view_revoked_certificates', methods=['GET'])
def view_revoked_certificates():
	if 'uid' in session:
		uid = session['uid']

		# revoked_certificates = db_funcs.get_revoked_certificates()
		# revoked_certificates = ca_query.get_revoked_certificates(uid)
		serial_list = db_funcs.get_certificates_id_by_uid(uid)
		# print(serial_list)
		revoked_certificates = ca_query.get_revoked_certificates_by_serial(serial_list)
		app.logger.info('User {} accessed the revoked certificate page.'.format(uid))
		return render_template('view_revoked_certificates.html', revoked_certificates=revoked_certificates)

	app.logger.info('Someone redirected to login credential page.')
	return redirect(url_for('login_credential'))

@app.route('/edit_profile', methods=['GET', 'POST'])
def edit_profile():
	if 'uid' in session:
		uid = session['uid']
		if request.method == 'GET':
			uid, firstname, lastname, email = db_funcs.get_profile_by_uid(uid)
			context = {
				'uid': uid,
				'firstname': firstname,
				'lastname': lastname,
				'email': email
			}
			app.logger.info('User {} accessed the edit profile page.'.format(uid))
			return render_template('edit_profile.html', context=context)
		elif request.method == 'POST':
			firstname, lastname, email = request.form['firstname'], request.form['lastname'], request.form['email']
			db_funcs.update_profile_by_uid(uid, firstname, lastname, email)
			app.logger.info('User {} updated the profile with: first name: {}, last name: {}, email: {}.'.format(uid, firstname, lastname, email))
			return redirect(url_for('edit_profile'))

	app.logger.info('Someone redirected to login credential page.')
	return redirect(url_for('login_credential'))

@app.route('/get_updated_ca_cert', methods=['GET'])
def get_update_ca_cert():
	if 'uid' in session:
		uid = session['uid']

		if os.path.exists(REVOKED_CERT_FILE):
			download = send_file(REVOKED_CERT_FILE, as_attachment=True)

			app.logger.info('User {} got the updated CA certificate.'.format(uid))

			return download

		else:
			app.logger.info('User {} requested the updated CA certificate but failed'.format(uid))

	app.logger.info('Someone redirected to login credential page.')
	return redirect(url_for('login_credential'))

@app.route('/logout', methods=['GET'])
def logout():
	if 'uid' in session:
		uid = session['uid']
		app.logger.info('User {} logged out.'.format(uid))
	session.clear()
	return redirect(url_for('login_credential'))

@app.route('/backup', methods=['GET'])
def backup():
	if request.remote_addr != BACKUP_SERVER_IP:
		app.logger.info('An invalid server has request for backup, be cautious.')
		abort(404)

	log_dict = {}
	with open(LOG_FILE, 'rb') as log_file:
		log_content = log_file.read()
	log_dict['log'] = log_content
	app.logger.info('Log was updated to the backup server.')

	return jsonify(log_dict)

if __name__ == '__main__':

	# app.run(host="0.0.0.0")

	app.run(host="webserver.imovies.ch", port=5000, ssl_context=(WEB_SERVER_CERT_FILE, WEB_SERVER_KEY_FILE))
